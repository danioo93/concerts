import Head from 'next/head';
import dateFormat from 'dateformat';

import { client } from '../utils/supabase';
import Header from '../components/Header';

export default function Home({ loading, concerts, error }) {
  return (
    <div>
      <Head>
        <title>Concerts</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <Header />

        {loading && (
          <p>Loading...</p>
        )}
        {error && (
          <p>Error!</p>
        )}
        {concerts && (
          <div className="flex items-center justify-center">
            {concerts.map((concert) => (
              <div className="max-w-md p-8 m-3 sm:flex sm:space-x-6 bg-gray-100 text-gray-800" key={concert.id}>
                <div className="flex-shrink-0 w-full mb-6 h-44 sm:h-32 sm:w-32 sm:mb-0">
                  <img src="https://source.unsplash.com/100x100/?portrait" alt="" className="object-cover object-center w-full h-full rounded" />
                </div>
                <div className="flex flex-col space-y-4">
                  <div>
                    <h2 className="text-2xl font-semibold">{concert.artist.name}</h2>
                    <span className="text-sm dark:text-coolGray-400">{concert.name}</span>
                  </div>
                  <div className="space-y-1">
                    <span className="flex items-center space-x-2">
                    <svg className="h-5 w-5 text-gray-600 mr-3" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                      <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z" />
                      <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M15 11a3 3 0 11-6 0 3 3 0 016 0z" />
                    </svg>
                      <span className="dark:text-coolGray-400">{concert.place.name}</span>
                    </span>
                    <span className="flex items-center space-x-2">
                      <svg className="h-5 w-5 text-gray-600 mr-3" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
                      </svg>
                      <span className="dark:text-coolGray-400">{dateFormat(concert.date, "dddd, mmmm d, yyyy h:MM TT")}</span>
                    </span>
                  </div>
                </div>
              </div>
            ))}
          </div>
        )}
      </main>
    </div>
  )
}

export async function getStaticProps() {
  const { data, error } = await client.
    from("concert").
    select("id, name, date, artist(id, name), place(id, name)")
    .range(0, 25)

  console.log(data, error)

  return {
    props: {
      concerts: data,
      loading: (!data && !error) || false,
      error: error || null
    }
  }
}
