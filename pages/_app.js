import { SupabaseContextProvider } from 'use-supabase';

import "tailwindcss/tailwind.css";

import { client } from '../utils/supabase';

import '../styles/globals.css'

function MyApp({ Component, pageProps }) {
  return (
    <SupabaseContextProvider client={client}>
      <Component {...pageProps} />
    </SupabaseContextProvider>
  )
}

export default MyApp
